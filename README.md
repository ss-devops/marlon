# Tecnologias utilizadas
- Spring Boot
- Spring Batch

# Pré-requisitos
- Java (versão 8 ou mais atual, testado com 1.8.0_112)

# Desenvolvimento local

## Rodando os testes
```
mvn clean install
```

## Rodando a aplicação
```
java -jar target/<app-name>-1.0.0.jar

# Rodando a aplicação em modo de produção
```
java -jar target/<app-name>-1.0.0.jar --spring.profiles.default=prod
```
