package br.com.southsystem.repository;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import br.com.southsystem.domain.Transaction;

@Repository
public interface TransactionRepository extends MongoRepository<Transaction, String> {

}
