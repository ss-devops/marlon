package br.com.southsystem.service;

import java.util.ArrayList;
import java.util.Arrays;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.stereotype.Service;

import br.com.southsystem.domain.Transaction;
import br.com.southsystem.repository.TransactionRepository;

@Service
@Transactional
public class TransactionService {
	private static final Logger log = LoggerFactory.getLogger(TransactionService.class);
	private final TransactionRepository transactionRepository;

	public TransactionService(TransactionRepository transactionRepository) {
		this.transactionRepository = transactionRepository;
	}
	

	public Transaction add(Transaction transaction) {
		return transactionRepository.save(transaction);
	}

}
